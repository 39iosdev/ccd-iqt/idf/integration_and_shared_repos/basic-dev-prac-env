#!/bin/bash

echo "This will install the following programs:"
echo "\tPython3"
echo "\tgit"
echo "\tSoftware necessary for Docker Engine: ca-certificates, curl, gnupg and lsb-release"
echo "\tDocker Engine"
echo "\tDocker Compose"
echo "\tgdebi (to ensure any prerequisites are also installed when using .deb files)"
echo "\tGoogle Chrome"
echo "Do you want to continue (y|n)? "
read answer

if [[ $(echo "$answer" | tr '[:upper:]' '[:lower:]') != "y" ]]
then
    echo "Aborting script."
    exit 0
fi

# Update the package manager
sudo apt-get update

# Ensure python 3 and git are installed
sudo apt-get install -y python3 git

# Ensure previous versions of Docker are removed
sudo apt-get remove -y docker docker-engine docker.io containerd runc

# Ensure necessary software is installed to perform Docker Engine installation
sudo apt-get install -y ca-certificates curl gnupg lsb-release

# Add Docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Setup the Docker repository in the package manager
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Now update the package manager with the newly added repository
sudo apt-get update

# Install Docker Engine
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Verify Docker Engine is installed correctly
sudo docker run hello-world

# Ensure the docker group exists
sudo groupadd docker

# Add the current user to the docker group
sudo usermod -aG docker $USER

# Ensure Docker Compose is installed. First download the binary
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Now apply execute permissions
sudo chmod +x /usr/local/bin/docker-compose

# Create a soft link to /usr/bin
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# Verify Docker Compose installed correctly
docker-compose --version

# Move here to store the Google Chrome installation file
cd /tmp

# Download and install Google Chrome
curl "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb" --output google-chrome-stable_current_amd64.deb
sudo apt install -y gdebi-core # Ensures any prerequisite software is also installed
sudo gdebi google-chrome-stable_current_amd64.deb

# Move back to the user's working directory
cd -

echo "'$USER' was added to the 'docker' group. Please log off and back on so your group membership is re-evaluated."