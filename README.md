# Table of Contents
- [CCD Performance Practice Environment](#ccd-performance-practice-environment)
  - [Setup Step 1. Ensure prerequisite software is installed](#setup-step-1-ensure-prerequisite-software-is-installed)
    - [Ubuntu](#ubuntu)
    - [Windows 10+](#windows-10)
      - [Windows Troubleshooting](#windows-troubleshooting)
    - [Apple macOS](#apple-macos)
      - [macOS Troubleshooting](#mac-troubleshooting)
  - [Setup Step 2. Practice Environment Setup](#setup-step-2-practice-environment-setup)
  - [Generating a New Practice test](#generating-a-new-practice-test)
    - [createPracticeTest.py](#createpracticetestpy)
  - [Building/Compiling/Running Questions](#buildingcompilingrunning-questions)
    - [Networking Questions](#networking-questions)
  - [VS Code Server Tasks](#vs-code-server-tasks)
  - [Features](#features)
  - [Sources](#sources)
- [How to Contribute](#how-to-contribute)


# CCD Performance Practice Environment
The Cyber Capability Developer practice environment runs in a VS Code Server docker container on your local host. When
running, a web server is hosted within this container and can be accessed using `https://127.0.0.1:8080`. You have the
option of generating a mock evaluation or a custom configuration of your choice.

\[ [TOC](#table-of-contents) \]


## Setup Step 1. Ensure prerequisite software is installed
The following are the necessary software components to use the VS Code Server environment. Operating-system specific
instruction are provided in the subsections below.

- Python 3.7+
- git
- Docker
- Docker-compose
- Chromium based web browser (such as Google Chrome)

\[ [TOC](#table-of-contents) \]


### Ubuntu
A script called `"install_prerequisites_ubuntu.sh"` is provided that will install all prerequisite software. To run the
script, use the `"git clone"` command in [step 2](#setup-step-2-practice-environment-setup) then run this script. If 
you would rather install everything manually, follow the steps below.

- Ensure [Python 3.7+](https://www.python.org/downloads/) is installed.
  ```
  python3 --version
  ```

- Ensure [git](https://www.atlassian.com/git/tutorials/install-git) is installed.
  ```
  git --version
  ```

- Ensure [Docker](https://docs.docker.com/get-docker/) is installed.
  ```
  docker --version
  ```

- Ensure [Docker Compose](https://docs.docker.com/compose/install/) is installed.
  ```
  docker-compose --version
  ```

- Ensure a [Chromium-based](https://support.google.com/chrome/) browser is installed.

### Windows 10+
- Ensure your machine is running Windows 10, [updated to version 2004](ms-settings:windowsupdate), Build 18362 or
  higher.

- Create a global WSL config file to control memory and CPU resources (optional but highly recommended for systems with
  low resources)

  - Within your user directory, create a file called `".wslconfig"` (i.e. c:\\Users\\MyUser\\.wslconfig)
  - Add the following and save the file.
    ```text
    # Apply settings to WSL 2
    [wsl2]

    # Set memory resource limits
    memory=4GB # Adjust as necessary

    # Set CPUs resource limits
    processors=2 # Adjust as necessary but you should not set it below 2 CPUs as some questions require more than one
    ```

- [Install WSL and your Linux distribution](https://docs.microsoft.com/en-us/windows/wsl/install).

  - After your computer reboots from installing WSL, set WSL 2 as the default for all WSL installations.
    ```
    wsl.exe --set-default-version 2
    ```
  - Verify your distribution is using WSL 2.
    ```
    wsl.exe -l -v
    ```
  - If necessary, upgrade your distro to use WSL 2. Note, your WSL instance must be off and the following command must 
    be run from a Windows command or PowerShell prompt. Replace \<distro name\> with the actual name of the 
    distribution.
    ```
    wsl.exe --set-version <distro name> 2
    ```
  - If necessary, set your distribution to be the default.
    ```
    wsl.exe --set-default <distro name>
    ```

- [Install Docker Desktop](https://docs.microsoft.com/en-us/windows/wsl/tutorials/wsl-containers#install-docker-desktop).

  - If a window appears asking for access through the firewall, click the button to allow the program through the 
    firewall.
  - Non-Administrator user accounts need to be added to the `"docker-users"` group.
  - Open a command prompt with administrator access.
  - Run the following command to add the currently logged-in user to the `"docker-users"` group.
    ```
    net localgroup "docker-users" "%username%" /add
    ```
  - Logout and back in so your account can be updated with the new permissions.

- Ensure a [Chromium-based](https://support.google.com/chrome/) browser is installed.

\[ [TOC](#table-of-contents) \]


#### Windows Troubleshooting
1. Your WSL distribution reports `docker` and `docker-compose` not being installed, and you have enabled WSL2 in Docker
   Desktop.

   - Restart Docker Desktop using one of the following:
     - Right click the Docker Desktop icon next to the clock on the task bar, OR
     - Restart the `Docker Desktop` service from `services.msc`

2. You receive the error, `'Error response from daemon: OCI runtime create failed: container_linux.go:380: starting
   container process caused: ... mounting "/run/desktop/mnt/host/wsl/docker-desktop-bind-mounts/Ubuntu-20.04/...'` when
   running `'docker-compose up -d'`:

   - Remove all docker containers starting with `"basic-dev-prac-env-"` and then run `setup.py` again.
     ```
     docker container ls --all
     docker container rm <container1-name> <container2-name> ...
     ```

3. You receive the error, `"Error response from daemon: not a directory"` when running 'docker-compose up -d':
   - Open `services.msc`
   - Restart `LxssManager` and `Docker Desktop`
   - Run `setup.py` again

\[ [TOC](#table-of-contents) \]

### Apple macOS
NOTE: At this time we do not support running the practice environment on ARM-based Mac systems, such as M1, M1 Pro,
etc. Our suggested workaround is to use an x86-based machine. You are welcome to try your own workarounds, such as
following the below instructions directly in macOS, but they may not work, particularly since the container and
networking binaries are Debian x86_64.

- Ensure [Python 3.7+](https://www.python.org/downloads/) is installed. You can use the Mac package manager 
  [homebrew](https://brew.sh/), or install Python from python.org. If Python 3 is not already installed on your system, 
  the command `python3` will cause macOS to prompt you to download the Xcode developer tools, which also include `git`. 
  Run the following to check your installation and version:
  ```
  python3 --version
  ```

- Ensure [git](https://www.atlassian.com/git/tutorials/install-git) is installed.  The `homebrew` version should work
  if you use `homebrew`.
  ```
  git --version
  ```

- Ensure [Docker](https://docs.docker.com/get-docker/) is installed. For `homebrew`, you will need to use the following 
  command due to a recent change in `homebrew`'s `cask` system. (See
  https://www.cprime.com/resources/blog/docker-for-mac-with-homebrew-a-step-by-step-tutorial/ for more details on
  `homebrew` and `docker`.)
  ```
  homebrew install --cask docker
  ```

- Whether you use `homebrew` or not, you will need to start up Docker before `docker` is available in the system path.
  Use Launchpad or Spotlight to open the application named `Docker`. Then use the following command to verify it is
  available in the path:
  ```
  docker --version
  ```

- Ensure [Docker Compose](https://docs.docker.com/compose/install/) is installed.
  ```
  docker-compose --version
  ```

- Ensure a [Chromium-based](https://support.google.com/chrome/) browser is installed.
  - Ensure that your chosen Chromium-based browser is your default browser. When you first open Chrome, there will be a
    prompt asking if you want to make it your default browser. If you are not prompted when you open Chrome, you can 
    go to `System Preferences` (select it from the Apple icon menu on the menubar), then select `General` and pick 
    Chrome in the dropdown list presented when you click on `Default Web Browser`. If you do not wish to set Chrome as 
    your default browser, you can open the URL in Google Chrome yourself. Copy and paste the URL (e.g., 
    `https://127.0.0.1:8080`) into Chrome.  (Note: Safari is *not* supported for the practice environment.)

\[ [TOC](#table-of-contents) \]

#### Mac Troubleshooting
1. If you receive an error like the following
   ```
   xcrun: error: invalid active developer path(/Library/Developer/CommandLineTools), missing xcrun at:
   /Library/Developer/CommandLineTools/usr/bin/xcrun
   ```
   - Your command-line developer tools need to be updated for your current OS. Run `xcode-select --install`.
   - If you still get the same error afterwards, try `sudo xcode-select --reset`.

2. If you get an error running `./setup.py` below, make sure you have Docker running. You may need to start
   `Docker Desktop` manually. You will know it is running when you see a small whale carrying containers in your icon
   bar.

3. If you get a port 5000 in use error like the below:
   ```
   ERROR: for basic-dev-prac-env_network-server_1 Cannot start service network-server: Ports are not available: listen 
   tcp 0.0.0.0:5000: bind: address    already in use
   ERROR: for network-server Cannot start service network-server: Ports are not available: listen tcp 0.0.0.0:5000: 
   bind: address already in use
   ```
   - You will need to disable `AirPlay Receiver` in the `Sharing` panel of `System Preferences`. (Recent versions of 
     macOS act as receivers for AirPlay, listening on port 5000 for this.)

   - If this continues to be a problem after you have disabled the `AirPlay Receiver` functionality, use the following
     command to identify what is listening on port 5000:
     ```
     lsof -nP +c 15 | grep LISTEN
     ```

4. If you get an error saying `docker` is not in your path, or an error saying that `docker-compose returned non-zero
   exit status`, start up the `Docker` application from Launchpad or Spotlight. This creates the symlinks in the path for
   `docker` and its commands.

5. If you get an error from Safari saying it can't access the URL, please see the last step in the Mac setup. Safari
   is not supported for the practice and testing environments.

\[ [TOC](#table-of-contents) \]


## Setup Step 2. Practice Environment Setup
With the prerequisite software installed, you just need to run the following commands and you'll be in the practice
environment. After some resources are downloaded and the containers start, your browser should open to the link where
the environment is hosted. To access the site, enter the password available in `"practice-exam/config.yaml"`.

```sh
git clone https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env
cd basic-dev-prac-env
./setup.py
```

\[ [TOC](#table-of-contents) \]


## Generating a New Practice test
Once the environment is all set up and ready to go, you are ready to start practicing using the previously generated
questions. To get a new set of questions, run the following command. WARNING: All previously worked on questions will be
deleted, so save any work you do not want to lose.

```sh
./createPracticeTest.py
```

\[ [TOC](#table-of-contents) \]


### createPracticeTest.py
This program allows you to generate a new set of practice questions. Without any parameters, it will generate a mock
evaluation setup. If you would like to generate a different configuration, check the `"--help"` option for details. The
output of the `"--help"` option is below. An undocumented option is specifying `all` by itself to get all available
questions; as in `createPracticeTest.py all`. Note, it may take up to 30 seconds for your browser to refresh after
running this command.

```text
usage: createPracticeTest.py [-h] [--list_topics] [question_amounts [question_amounts ...]]

positional arguments:
 question_amounts A repeating list in the format: Topic number e.g. Python 5 C all

optional arguments:
 -h, --help  show this help message and exit
 --list_topics  get a list of available topics
```

\[ [TOC](#table-of-contents) \]


## Building/Compiling/Running Questions
Follow the [compile-instructions.md](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md) 
once your environment is staged and ready to go. While in VSCode and focusing on the file, press `ctrl+shift+v`. This 
will open up a new tab with the rendered markdown.

\[ [TOC](#table-of-contents) \]


### Networking Questions
Each networking question comes with a `"run_server_debian"` binary file. Prior to running your test code, run this file
in a separate terminal. When running your client code, ensure to change the host address from `"network-server"` to
`"127.0.0.1"`.

\[ [TOC](#table-of-contents) \]


## VS Code Server Tasks
Click the button labeled "Tasks" in the bottom blue bar to view tasks to execute. Alternatively, press `ctrl + p` and 
type `'task '`. Don't forget the trailing space and don't include quotes. These tasks are only for C Programming. The 
task hierarchy is the following.

```sh
└─ Run C Debug Tests
| └─ Compile C Debug
│  └─ Build C Debug
|
└─ Run C Release Tests
| └─ Compile C Release
│  └─ Build C Release
|
└─ Clean 
```

\[ [TOC](#table-of-contents) \]


### Features
- Lightweight and open source C and Python development environment

- C build and compilation with CMake

- Debugging for C and Python
 - gdb debugging for C through VSCode GUI
 - Breakpoints, step through, etc.
 - Variable and Call Stack analysis

- Docker
 - Easy to deploy containerized dev environment
 - Wraps all extensions, libraries, and tools
 - It can be personalized

\[ [TOC](#table-of-contents) \]


### Sources
- https://cmake.org/cmake/help/v3.13/
- https://code.visualstudio.com/docs/remote/containers
- https://code.visualstudio.com/docs/remote/containers-advanced
- https://code.visualstudio.com/docs/remote/wsl

\[ [TOC](#table-of-contents) \]


# How to Contribute
There are many ways with different levels of involvement one can have to make a contribution.

1. If you encounter a problem anywhere, create an [issue ticket](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/basic-dev-prac-env/-/issues/new)   
   on this repo. This will highlight the problem to the community and act as a focal point to track progress as our 
   team resolves it.

2. Create an [issue ticket](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/basic-dev-prac-env/-/issues/new), 
   create a branch from the ticket, fix the problem, and submit a merge request. Our team will analyze the changes and 
   merge it into the project. 

Our team is small and working on many different projects. Your contribution will help tremendously.

\[ [TOC](#table-of-contents) \]
